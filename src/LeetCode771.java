/**
 * @author huangxunhui
 * Date: Created in 2020/7/17 3:58 下午
 * Utils: Intellij Idea
 * Description: 771. 宝石与石头
 */
public class LeetCode771 {

    class Solution {
        public int numJewelsInStones(String J, String S) {
            int result = 0;
            for (char s : S.toCharArray()) {
                for (char j : J.toCharArray()) {
                    if (s == j) {
                        result++;
                    }
                }
            }
            return result;

        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode771().new Solution();
        System.out.println(solution.numJewelsInStones("aA", "aAAbbbb"));
    }

}
