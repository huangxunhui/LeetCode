/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 2:38 下午
 * Utils: Intellij Idea
 * Description: 1299. 将每个元素替换为右侧最大元素
 */
public class LeetCode1299 {

    class Solution {
        public int[] replaceElements(int[] arr) {
            for (int i = 0; i < arr.length; i++) {
                int max = -1;
                for (int j = i + 1; j < arr.length; j++) {
                    if(max < arr[j]){
                        max = arr[j];
                    }
                }
                arr[i] = max;
            }
            return arr;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1299().new Solution();
        int[] arr = {17,18,5,4,6,1};
        int[] elements = solution.replaceElements(arr);
        for (int element : elements) {
            System.out.println("i = " + element);
        }
    }
}
