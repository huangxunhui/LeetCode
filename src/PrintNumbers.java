/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 2:10 下午
 * Utils: Intellij Idea
 * Description: 剑指 Offer 17. 打印从1到最大的n位数
 */
public class PrintNumbers {

    class Solution {
        public int[] printNumbers(int n) {
            double length = Math.pow(10, n) - 1;
            int [] result = new int[(int) length];
            for (int i = 0; i < length; i++) {
                result[i] = i + 1;
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new PrintNumbers().new Solution();
        for (int i : solution.printNumbers(1)) {
            System.out.println("i = " + i);
        }
    }

}
