import javax.xml.soap.Node;

/**
 * @author huangxunhui
 * Date: Created in 18/10/17 下午9:10
 * Utils: Intellij Idea
 * Description:
 */
public class AddTwoNumbers_01 {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        StringBuilder l1_num = new StringBuilder();
        StringBuilder l2_num = new StringBuilder();

        while(l1 != null && l2 !=null){
            l1_num.append(l1.val);
            l2_num.append(l2.val);
            l1 = l1.next;
            l2 = l2.next;
        }

        String result =  (Long.parseLong(l1_num.toString()) + Long.parseLong(l2_num.toString()))+"";
        System.out.println("result = " + result);
        ListNode resultNode = new ListNode(Integer.parseInt(result.charAt(result.length() - 1)+""));
        ListNode cur = resultNode;
        for (int i = result.length() - 2; i >= 0 ; i --) {
            System.out.println("i = " + i);
            cur.next = new ListNode(Integer.parseInt(result.charAt(i)+""));
            cur = cur.next;
        }
        return resultNode;
    }

    public static void add(ListNode node , int[] nums){
        for (int i = 1; i < nums.length ; i++) {
            node.next = new ListNode(nums[i]);
            node = node.next;
        }
    }

    public static void main(String[] args) {
        int[] nums1 = {9,9,9,9,9,9,9,9,9};
        int[] nums2 = {9,9,9,9,9,9,9,9,9};

        ListNode l1 = new ListNode(nums1[0]);
        add(l1,  nums1);

        ListNode l2 = new ListNode(nums2[0]);
        add(l2,  nums2);

        System.out.println("l2 = " + l2);

        ListNode listNode = new AddTwoNumbers_01().addTwoNumbers(l1, l2);
        System.out.println("listNode = " + listNode);

    }

}
