/**
 * @author huangxunhui
 * Date: Created in 2020/7/19 10:22 下午
 * Utils: Intellij Idea
 * Description: 整数的各位积和之差
 */
public class LeetCode1281 {

    public static void main(String[] args) {
        System.out.println(Solution.subtractProductAndSum(4421));
    }

    static class Solution {
        public static int subtractProductAndSum(int n) {
            int sum = 0;
            int product = 1;
            while (n > 0) {
                sum += n % 10;
                product *= n % 10;
                n /= 10;
            }
            return product - sum;
        }
    }

}
