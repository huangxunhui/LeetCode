import java.util.*;

/**
 * @author huangxunhui
 * Date: Created in 2020/7/17 5:08 下午
 * Utils: Intellij Idea
 * Description:
 */
public class LeetCode1431 {

    class Solution {
        public List<Boolean> kidsWithCandies(int[] candies, int extraCandies) {
            List<Boolean> result = new ArrayList<>(candies.length);
            int max = 0;

            for (int candy : candies) {
                max = Math.max(candy,max);
            }

            for (int candy : candies) {
                result.add(candy + extraCandies >= max);
            }

            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1431().new Solution();
        int[] candies = {2,8,7};
        System.out.println(solution.kidsWithCandies(candies,1));
    }

}
