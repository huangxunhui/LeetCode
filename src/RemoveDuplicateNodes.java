import java.util.Set;
import java.util.TreeSet;

/**
 * @author huangxunhui
 * Date: Created in 2020/7/24 4:09 下午
 * Utils: Intellij Idea
 * Description: 面试题 02.01. 移除重复节点
 */
public class RemoveDuplicateNodes {

    class Solution {
        public ListNode removeDuplicateNodes(ListNode head) {
            Set<Integer> set = new TreeSet<>();
            while(head != null){
                set.add(head.val);
                head = head.next;
            }

            ListNode result = null;
            ListNode tmp = null;

            for (Integer integer : set) {
                if(result == null){
                    result = new ListNode(integer);
                    tmp = result;
                }else {
                    tmp.next = new ListNode(integer);
                    tmp = tmp.next;
                }
            }
            return result;
        }
    }

    public static void main(String[] args) {

    }

}
