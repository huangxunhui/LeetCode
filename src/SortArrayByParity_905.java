/**
 * @author huangxunhui
 * Date: Created in 18/10/18 上午11:48
 * Utils: Intellij Idea
 * Description: 按奇偶排序数组
 */
public class SortArrayByParity_905 {

    public int[] sortArrayByParity(int[] A) {
        int[] result = new int[A.length];
        int top = 0;
        int tail = A.length - 1;
        for (int i = 0; i < A.length ; i++) {
            if(A[i]%2 == 0){
                result[top] = A[i];
                top ++;
            }else {
                result[tail] = A[i];
                tail--;
            }

        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = {3,1,2,4};

        int[] array = new SortArrayByParity_905().sortArrayByParity(nums);

        for (int i = 0; i < array.length ; i++) {
            System.out.println("array = " + array[i]);
        }
    }

}
