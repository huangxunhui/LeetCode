import java.util.HashMap;
import java.util.Map;

/**
 * @author huangxunhui
 * Date: Created in 2020/7/16 3:32 下午
 * Utils: Intellij Idea
 * Description: 好数对的数目
 */
public class LeetCode1512 {

    class Solution {
        public int numIdenticalPairs(int[] nums) {
            Map<Integer,Integer> map = new HashMap<>(101);
            int ret = 0;
            // 扫描一遍取出相同的数据
            for (int num : nums) {
                if (map.containsKey(num)) {
                    Integer result = map.get(num) + 1;
                    map.put(num, result);
                } else {
                    map.put(num, 1);
                }
            }

            for (Integer value : map.values()) {
                ret += value * (value - 1) / 2;
            }

            return ret;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1512().new Solution();
        int[] nums = {1,1,1,1};
        System.out.println(solution.numIdenticalPairs(nums));
    }

}
