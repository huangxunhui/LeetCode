/**
 * @author huangxunhui
 * Date: Created in 18/10/18 下午3:53
 * Utils: Intellij Idea
 * Description: 按奇偶排序数组 II
 */
public class SortArrayByParityII_922 {

    public int[] sortArrayByParityII(int[] A) {
        int[] result = new int[A.length];
        int top = 0;
        int tail = 1;
        for (int i = 0; i < A.length ; i++) {
            if(A[i]%2 == 0){
                result[top] = A[i];
                System.out.println("result top [" + top + "] : " + A[i]);
                top = top + 2;
            }else{
                result[tail] = A[i];
                System.out.println("result tail [" + tail + "] : " + A[i]);
                tail = tail + 2;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = {4,2,5,7};

        int[] array = new SortArrayByParityII_922().sortArrayByParityII(nums);

        for (int i = 0; i < array.length ; i++) {
            System.out.println("array = " + array[i]);
        }
    }
}
