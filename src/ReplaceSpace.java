/**
 * @author huangxunhui
 * Date: Created in 2020/7/23 10:16 上午
 * Utils: Intellij Idea
 * Description: 剑指 Offer 05. 替换空格
 */
public class ReplaceSpace {

    class Solution {
        public String replaceSpace(String s) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == ' ') {
                    sb.append("%20");
                } else {
                    sb.append(s.charAt(i));
                }
            }
            return sb.toString();
        }
    }

    public static void main(String[] args) {
        Solution solution = new ReplaceSpace().new Solution();
        System.out.println(solution.replaceSpace("We are happy."));
    }

}
