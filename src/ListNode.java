/**
 * @author huangxunhui
 * Date: Created in 18/10/9 上午11:48
 * Utils: Intellij Idea
 * Description:
 */
public class ListNode {

    int val;

    ListNode next;

    ListNode(int x) { val = x; }


    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        //使用while循环遍历
        ListNode cur = this;
        while (cur != null){
            res.append(cur.val + "->");
            cur = cur.next;
        }
         res.append("NULL");
        return res.toString();
    }
}
