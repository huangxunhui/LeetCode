/**
 * @author huangxunhui
 * Date: Created in 18/10/18 下午1:58
 * Utils: Intellij Idea
 * Description:
 */
public class MergeTwoLists_21 {

    public static void add(ListNode node, int[] nums) {
        for (int i = 1; i < nums.length; i++) {
            node.next = new ListNode(nums[i]);
            node = node.next;
        }
    }

    public static void main(String[] args) {

        int[] nums1 = {5};

        int[] nums2 = {1, 2, 4};

        ListNode l1 = new ListNode(nums1[0]);
        add(l1, nums1);

        ListNode l2 = new ListNode(nums2[0]);
        add(l2, nums2);


        ListNode listNode = new MergeTwoLists_21().mergeTwoLists(l1, l2);
        System.out.println("listNode = " + listNode);

    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode dummyHead = new ListNode(-1);
        ListNode result;

        if (l1 == null && l2 == null) {
            return null;
        }
        if (l1 != null && l2 == null) {
            return l1;
        }

        if (l2 != null && l1 == null) {
            return l2;
        }

        while(l1 != null || l2 != null){

            l1 = l1.next;
            l2 = l1.next;

        }




        if (l1.val >= l2.val) {
            result = new ListNode(l2.val);
        } else {
            result = new ListNode(l1.val);
        }
        dummyHead.next = result;
        ListNode temp = dummyHead;


        return dummyHead.next;
    }

}
