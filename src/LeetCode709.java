/**
 * @author huangxunhui
 * Date: Created in 2020/7/22 5:53 下午
 * Utils: Intellij Idea
 * Description: 709. 转换成小写字母
 */
public class LeetCode709 {

    class Solution {
        public String toLowerCase(String str) {
            // 遍历字符串
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                char c = str.charAt(i);
                if(c >= 'A' && c <= 'Z'){
                     c = (char)(c + 32);
                }
                sb.append(c);
            }
            return sb.toString();
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode709().new Solution();
        System.out.println(solution.toLowerCase("LOVE"));
    }

}
