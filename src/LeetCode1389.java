/**
 * @author huangxunhui
 * Date: Created in 2020/7/23 2:41 下午
 * Utils: Intellij Idea
 * Description: 1389. 按既定顺序创建目标数组
 */
public class LeetCode1389 {

    public static void main(String[] args) {
        Solution solution = new LeetCode1389().new Solution();
        int[] nums = {0, 1, 2, 3, 4};
        int[] index = {0, 1, 2, 2, 1};
        int[] targetArray = solution.createTargetArray(nums, index);
        for (int i : targetArray) {
            System.out.println("i = " + i);
        }
    }

    class Solution {
        public int[] createTargetArray(int[] nums, int[] index) {
            int[] result = new int[index.length];
            for (int i = 0; i < index.length; i++) {
                // 位置拷贝
                if (result.length - 1 - index[i] >= 0) {
                    System.arraycopy(result, index[i], result, index[i] + 1, result.length - 1 - index[i]);
                }
                // 数据添加
                result[index[i]] = nums[i];
            }
            return result;
        }
    }

}
