/**
 * @author huangxunhui
 * Date: Created in 2020/7/23 6:17 下午
 * Utils: Intellij Idea
 * Description: 1491. 去掉最低工资和最高工资后的工资平均值
 */
public class LeetCode1491 {

    public static void main(String[] args) {
        Solution solution = new LeetCode1491().new Solution();
        int[] salary = {48000,59000,99000,13000,78000,45000,31000,17000,39000,37000,93000,77000,33000,28000,4000,54000,67000,6000,1000,11000};
        System.out.println(solution.average(salary));
    }

    class Solution {
        public double average(int[] salary) {
            double max = 0;
            double min = salary[0];
            double sum = 0;

            for (int i : salary) {
                if(i > max){
                    max = i;
                }

                if(i < min){
                    min = i;
                }

                sum += i;
            }

            return (sum - min - max) / (salary.length - 2);

        }
    }

}
