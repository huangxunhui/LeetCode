/**
 * @author huangxunhui
 * Date: Created in 18/9/20 下午7:42
 * Utils: Intellij Idea
 * Description: 两数之和
 */
public class TwoSum_01 {

    private int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length ; j++) {
                if(nums[i]+nums[j] == target){
                    result[0] = i ;
                    result[1] = j ;
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] nums = {2, 7, 11, 15};
        int target = 13;
        new TwoSum_01().twoSum(nums, target);
    }
}

