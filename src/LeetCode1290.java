/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 9:54 上午
 * Utils: Intellij Idea
 * Description: 二进制链表转整数
 */
public class LeetCode1290 {

    class Solution {
        public int getDecimalValue(ListNode head) {
            int result = 0;
            while (head != null) {
                result = result * 2 + head.val;
                head = head.next;
            }
            return result;
        }
    }

    public static void main(String[] args) {
        ListNode listNode = new ListNode(1);
        ListNode listNode1 = new ListNode(0);
        ListNode listNode2 = new ListNode(0);
        ListNode listNode3 = new ListNode(1);

        listNode.next = listNode1;
        listNode1.next = listNode2;
        listNode2.next = listNode3;

        Solution solution = new LeetCode1290().new Solution();
        System.out.println(solution.getDecimalValue(listNode));
    }

}
