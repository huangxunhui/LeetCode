/**
 * @author huangxunhui
 * Date: Created in 2019-02-15 15:54
 * Utils: Intellij Idea
 * Description: 35. 搜索插入位置
 */
public class LeetCode_35 {

    class Solution {
        public int searchInsert(int[] nums, int target) {
            int ret_insert_index = 0;
            for (int i = 0; i < nums.length; i++) {
                if(nums[i] == target){
                    return i;
                }
                if(nums[i] > target){
                    break;
                }else{
                    ret_insert_index = i+1;
                }
            }
            return ret_insert_index;
        }
    }

    public static void main(String[] args) {
        LeetCode_35.Solution solution = new LeetCode_35().new Solution();
        int[] nums = {1,3,5,6};
        int i = solution.searchInsert(nums, 5);
        System.out.println("i = " + i);
    }
}
