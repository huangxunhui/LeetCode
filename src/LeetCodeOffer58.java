/**
 * @author huangxunhui
 * Date: Created in 2020/7/19 10:09 下午
 * Utils: Intellij Idea
 * Description: 左旋转字符串
 */
public class LeetCodeOffer58 {

    class Solution {
        public String reverseLeftWords(String s, int n) {
            return s.substring(n) + s.substring(0, n);
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCodeOffer58().new Solution();
        System.out.println(solution.reverseLeftWords("qwert", 1));
    }

}
