/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 3:21 下午
 * Utils: Intellij Idea
 * Description: 125. 验证回文串
 */
public class LeetCode125 {

    class Solution {
        public boolean isPalindrome(String s) {
            if(s == null){
                return true;
            }
            // 转换为大写
            s = s.toUpperCase();

            // 过滤后的字符串
            StringBuilder sb = new StringBuilder();
            for (char c : s.toCharArray()) {
                // 判断是否是小写之母和是否是数字
                if((c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9')){
                    sb.append(c);
                }
            }
            return sb.toString().equals(sb.reverse().toString());
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode125().new Solution();
        System.out.println(solution.isPalindrome("0P"));
    }

}
