/**
 * @author huangxunhui
 * Date: Created in 2020/7/22 9:44 上午
 * Utils: Intellij Idea
 * Description: 剑指 Offer 24. 反转链表
 */
public class ReverseList {

    class Solution {
        public ListNode reverseList(ListNode head) {

            if(head == null){
                return head;
            }

            //TODO 使用头插法进行优化
            ListNode curr = head;
            ListNode newHead = null;
            ListNode tmp = null;

            while (head != null){
                newHead = new ListNode(head.val);
                head = head.next;

            }

            // 返回结果
            return newHead;
        }
    }


    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(3);

        listNode1.next = listNode2;
        listNode2.next = listNode3;

        Solution solution = new ReverseList().new Solution();
        ListNode result = solution.reverseList(listNode1);
        System.out.println("result.toString() = " + result.toString());

    }

}
