import java.util.Arrays;

/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 2:28 下午
 * Utils: Intellij Idea
 * Description: 1502. 判断能否形成等差数列
 */
public class LeetCode1502 {


    public static void main(String[] args) {
        Solution solution = new LeetCode1502().new Solution();
        int[] arr = {3,4,1};
        System.out.println(solution.canMakeArithmeticProgression(arr));
    }

    class Solution {
        public boolean canMakeArithmeticProgression(int[] arr) {
            // 排序
            Arrays.sort(arr);

            // 验证是否为等差
            int tolerance = arr[1] - arr[0];
            for (int i = 2; i < arr.length; i++) {
                if ((arr[i] - arr[i - 1]) != tolerance) {
                    return false;
                }
            }
            return true;
        }
    }

}
