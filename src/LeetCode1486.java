/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 4:27 下午
 * Utils: Intellij Idea
 * Description: 1486. 数组异或操作
 */
public class LeetCode1486 {

    class Solution {
        public int xorOperation(int n, int start) {
            int result = 0;
            for (int i = 0; i < n; i++) {
                result ^= (start + 2 * i);
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1486().new Solution();
        System.out.println("solution.xorOperation(5,0) = " + solution.xorOperation(5, 0));
    }

}
