/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 11:03 上午
 * Utils: Intellij Idea
 * Description: 面试题 02.02. 返回倒数第 k 个节点
 */
public class KthToLast {

    class Solution {
        public int kthToLast(ListNode head, int k) {
            int result = 0;
            int length = 0;
            ListNode headTmp = head;

            // 获取链表长度
            while (headTmp != null) {
                length++;
                headTmp = headTmp.next;
            }

            for (int i = 0; i < length - k; i++) {
                result = head.val;
                head = head.next;
            }

            return result;
        }
    }

}
