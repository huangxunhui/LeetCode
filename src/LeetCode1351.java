/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 4:34 下午
 * Utils: Intellij Idea
 * Description: 1351. 统计有序矩阵中的负数
 */
public class LeetCode1351 {

    public static void main(String[] args) {
        Solution solution = new LeetCode1351().new Solution();

    }

    class Solution {
        public int countNegatives(int[][] grid) {
            int count = 0;
            for (int[] ints : grid) {
                for (int anInt : ints) {
                    if (anInt < 0) {
                        count++;
                    }
                }
            }
            return count;
        }
    }

}

