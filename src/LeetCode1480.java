/**
 * @author huangxunhui
 * Date: Created in 2020/7/16 2:05 下午
 * Utils: Intellij Idea
 * Description: 一维数组的动态和
 *
 */
public class LeetCode1480 {

    class Solution {
        public int[] runningSum(int[] nums) {
            int[] result = new int[nums.length];
            int sum = 0;
            for (int i = 0; i < nums.length; i++) {
                result[i] = sum + nums[i];
                sum += nums[i];
            }
            return result;
        }
    }

    public static void main(String[] args) {
        LeetCode1480.Solution solution = new LeetCode1480().new Solution();
        int[] nums = {1,1,1,1,1};
        for (int i : solution.runningSum(nums)) {
            System.out.println("i = " + i);
        }
    }
}
