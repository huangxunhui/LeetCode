import java.util.Arrays;

/**
 * @author huangxunhui
 * Date: Created in 18/10/18 下午4:13
 * Utils: Intellij Idea
 * Description: 合并两个有序数组
 */
public class Merge_88 {

    public void merge(int[] nums1, int m, int[] nums2, int n) {
        System.arraycopy(nums2, 0, nums1, m, n);
        Arrays.sort(nums1);
    }

    public static void main(String[] args) {
        int[] nums1 = {1,2,3,0,0,0};
        int m = 3;
        int[] nums2 = {2,5,6};
        int n = 3;

        new Merge_88().merge(nums1 , m ,nums2 , n);

        for (int i = 0; i < nums1.length; i++) {
            System.out.println("nums1 = " + nums1[i]);
        }
    }
}
