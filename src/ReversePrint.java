import java.util.ArrayDeque;

/**
 * @author huangxunhui
 * Date: Created in 2020/7/23 10:25 上午
 * Utils: Intellij Idea
 * Description:
 */
public class ReversePrint {

    class Solution {
        public int[] reversePrint(ListNode head) {

            //TODO 需要优化
            if(head == null){
                return new int[0];
            }

            ArrayDeque<Integer> deque = new ArrayDeque<>();

            while (head != null){
                deque.push(head.val);
                head = head.next;
            }

            int[] result = new int[deque.size()];
            for (int i = 0; i < result.length; i++) {
                result[i] = deque.pop();
            }

            return result;
        }
    }

}
