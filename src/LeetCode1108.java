/**
 * @author huangxunhui
 * Date: Created in 2020/7/17 2:15 下午
 * Utils: Intellij Idea
 * Description: IP 地址无效化
 */
public class LeetCode1108 {

    class Solution {
        public String defangIPaddr(String address) {

            return address.replace(".","[.]");
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1108().new Solution();
        System.out.println(solution.defangIPaddr("255.100.50.0"));
    }


}
