import java.util.HashSet;
import java.util.Set;

/**
 * @author huangxunhui
 * Date: Created in 2020/7/24 2:45 下午
 * Utils: Intellij Idea
 * Description: 
 */
public class IsUnique {

    class Solution {
        public boolean isUnique(String astr) {
            Set<Character> set = new HashSet<>();
            char[] chars = astr.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                set.add(chars[i]);
                if(i+1 != set.size()){
                    return false;
                }
            }
            return true;
        }
    }

    public static void main(String[] args) {
        Solution solution = new IsUnique().new Solution();
        String str = "abc";
        System.out.println(solution.isUnique(str));
    }

}
