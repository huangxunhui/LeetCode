/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 4:45 下午
 * Utils: Intellij Idea
 * Description: 1475. 商品折扣后的最终价格
 */
public class LeetCode1475 {

    class Solution {
        public int[] finalPrices(int[] prices) {
            for (int i = 0; i < prices.length; i++) {
                for (int j = i+1; j < prices.length; j++) {
                    if(prices[j] <= prices[i]){
                        prices[i] = prices[i] - prices[j];
                        break;
                    }
                }
            }
            return prices;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1475().new Solution();
        int[] prices = {10,1,1,6};
        for (int i : solution.finalPrices(prices)) {
            System.out.println(i);
        }

    }

}
