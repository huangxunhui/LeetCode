import java.util.Arrays;

/**
 * @author huangxunhui
 * Date: Created in 2019-02-15 17:57
 * Utils: Intellij Idea
 * Description: 561. 数组拆分 I
 */
public class LeetCode_561 {

    class Solution {
        public int arrayPairSum(int[] nums) {
            Arrays.sort(nums);
            int ret = 0;
            for (int i = 0; i < nums.length-1 ; i+=2) {
                ret += nums[i];
            }
            return ret;
        }
    }


    public static void main(String[] args) {
        Solution solution = new LeetCode_561().new Solution();

        int [] nums = {1,4,3,2};
        int i = solution.arrayPairSum(nums);
        System.out.println("i = " + i);

    }

}
