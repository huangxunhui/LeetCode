/**
 * @author huangxunhui
 * Date: Created in 2020/7/24 4:37 下午
 * Utils: Intellij Idea
 * Description: 509. 斐波那契数
 */
public class LeetCode509 {

    class Solution {
        public int fib(int N) {

            if (N <= 1) {
                return 0;
            }

            int result = 0;
            int zero = 0;
            int one = 1;
            for (int i = 1; i < N; i++) {
                result = zero + one;
                zero = one;
                one = result;
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode509().new Solution();
        System.out.println(solution.fib(4));
    }

}
