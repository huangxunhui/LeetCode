/**
 * @author huangxunhui
 * Date: Created in 2020/7/22 3:14 下午
 * Utils: Intellij Idea
 * Description: 解压缩编码列表
 */
public class LeetCode1313 {

    class Solution {
        public int[] decompressRLElist(int[] nums) {

            // 求出数组长度
            int length = 0;
            for (int i = 0; i < nums.length; i+=2) {
                length += nums[i];
            }

            int[] result = new int[length];

            int index = 0;
            for (int i = 0; i < nums.length; i+=2) {
                for (int j = 0; j < nums[i]; j++) {
                   result[index] = nums[i+1];
                   index ++;
                }
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1313().new Solution();
        int[] nums = {1,1,2,3};
        solution.decompressRLElist(nums);
    }

}
