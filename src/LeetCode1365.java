/**
 * @author huangxunhui
 * Date: Created in 2020/7/20 9:42 上午
 * Utils: Intellij Idea
 * Description: 有多少小于当前数字的数字
 */
public class LeetCode1365 {

    class Solution {
        public int[] smallerNumbersThanCurrent(int[] nums) {
            int[] result = new int[101];
            for (int i = 0; i < nums.length; i++) {
                int count = 0;
                for (int num : nums) {
                    if (nums[i] > num) {
                        count++;
                    }
                }
                result[i] = count;
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1365().new Solution();
        int[] nums = {8,1,2,2,3};
        for (int i : solution.smallerNumbersThanCurrent(nums)) {
            System.out.println("i = " + i);
        }
    }

}
