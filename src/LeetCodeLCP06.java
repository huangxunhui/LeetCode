/**
 * @author huangxunhui
 * Date: Created in 2020/7/17 4:09 下午
 * Utils: Intellij Idea
 * Description: LCP 06. 拿硬币
 */
public class LeetCodeLCP06 {

    class Solution {
        public int minCount(int[] coins) {
            int result = 0;
            for (int coin : coins) {
                result += coin / 2 + coin % 2;
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCodeLCP06().new Solution();
        int[] coins = {2, 3, 10};
        System.out.println(solution.minCount(coins));
    }

}
