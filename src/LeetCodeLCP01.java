/**
 * @author huangxunhui
 * Date: Created in 2020/7/17 3:49 下午
 * Utils: Intellij Idea
 * Description: LCP 01. 猜数字
 */
public class LeetCodeLCP01 {

    class Solution {
        public int game(int[] guess, int[] answer) {
            int result = 0;
            for (int i = 0; i < guess.length; i++) {
                if (guess[i] == answer[i]) {
                    result++;
                }
            }
            return result;
        }
    }

    public static void main(String[] args) {
        int[] guess = {1,2,3};
        int[] answer = {1,2,3};
        Solution solution = new LeetCodeLCP01().new Solution();
        System.out.println(solution.game(guess, answer));
    }

}
