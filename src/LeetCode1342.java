/**
 * @author huangxunhui
 * Date: Created in 2020/7/17 2:22 下午
 * Utils: Intellij Idea
 * Description:
 */
public class LeetCode1342 {

    class Solution {
        int result = 0;
        public int numberOfSteps (int num) {
            if(num == 0){
                return result;
            }
            result ++;
            return numberOfSteps(num % 2 == 0 ? num / 2 : num - 1);
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1342().new Solution();
        System.out.println("solution.numberOfSteps(14) = " + solution.numberOfSteps(14));
    }

}
