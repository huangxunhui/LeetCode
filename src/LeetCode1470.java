/**
 * @author huangxunhui
 * Date: Created in 2020/7/16 3:57 下午
 * Utils: Intellij Idea
 * Description: 重新排列数组
 */
public class LeetCode1470 {

    class Solution {
        public int[] shuffle(int[] nums, int n) {
            int[] result = new int[nums.length];
            int j = 0;
            for (int i = 0; i < nums.length / 2; i++ ) {
                result[j] = nums[i];
                result[j+1] = nums[n];
                n++;
                j+=2;
            }
            return result;
        }
    }

    public static void main(String[] args) {
        LeetCode1470.Solution solution =  new LeetCode1470(). new Solution();
        int[] nums = {2,5,1,3,4,7};
        for (int i : solution.shuffle(nums, 3)) {
            System.out.println(i);
        }
    }
}
