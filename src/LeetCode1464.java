/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 10:31 上午
 * Utils: Intellij Idea
 * Description: 数组中两元素的最大乘积
 */
public class LeetCode1464 {

    class Solution {
        public int maxProduct(int[] nums) {
            int max = nums[0];
            int secMax = nums[1];
            for (int i = 2; i < nums.length; i++) {
                if(nums[i] > max || nums[i] > secMax) {
                    if(max >= secMax) {
                        secMax = nums[i];
                    }else {
                        max = nums[i];
                    }
                }
            }
            return (secMax - 1) * (max - 1);
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1464().new Solution();
        int[] nums = {3, 4, 5, 2};
        System.out.println(solution.maxProduct(nums));

    }

}
