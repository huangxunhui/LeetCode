import java.util.Iterator;
import java.util.TreeSet;

/**
 * @author huangxunhui
 * Date: Created in 2019-02-15 14:07
 * Utils: Intellij Idea
 * Description: 26. 删除排序数组中的重复项
 */
public class LeetCode_26 {

    class Solution {
        public int removeDuplicates(int[] nums) {
            int p = 0;
            for (int i = 0; i < nums.length; i++) {
                if(nums[i]!=nums[p]){
                    nums[++p]=nums[i];
                }
            }
            return p+1;
        }
    }

    public static void main(String[] args) {

        LeetCode_26 leetCode_26 = new LeetCode_26();

        Solution solution  = leetCode_26.new Solution();

        int[] nums = {-3,-1,0,0,0,3,3};

        int i = solution.removeDuplicates(nums);

        System.out.println("i = " + i);


    }

}
