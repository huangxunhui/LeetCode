/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 9:37 上午
 * Utils: Intellij Idea
 * Description: 在既定时间做作业的学生人数
 */
public class LeetCode1450 {

    class Solution {
        public int busyStudent(int[] startTime, int[] endTime, int queryTime) {
            int count = 0;
            for (int i = 0; i < startTime.length; i++) {
                if (startTime[i] <= queryTime && endTime[i] >= queryTime) {
                    count++;
                }
            }
            return count;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1450().new Solution();
        int[] startTime = {4};
        int[] endTime = {4};
        System.out.println(solution.busyStudent(startTime, endTime, 5));
    }

}
