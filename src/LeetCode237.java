/**
 * @author huangxunhui
 * Date: Created in 2020/7/17 3:45 下午
 * Utils: Intellij Idea
 * Description:
 */
public class LeetCode237 {

    class Solution {
        public void deleteNode(ListNode node) {
            ListNode next = node.next;
            node.val = next.val;
            node.next = next.next;
        }
    }

    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public static void main(String[] args) {


    }

}
