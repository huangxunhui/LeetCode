/**
 * @author huangxunhui
 * Date: Created in 2020/7/22 6:07 下午
 * Utils: Intellij Idea
 * Description: 和为零的N个唯一整数
 */
public class LeetCode1304 {

    class Solution {
        public int[] sumZero(int n) {
            int[] result = new int[n];

            result[n-1] = - ( (n - 1) * (n) / 2);

            for (int i = 0; i < n - 1; i++) {
                 result[i] = i+1;
            }

            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1304().new Solution();
        int[] ints = solution.sumZero(5);
        for (int anInt : ints) {
            System.out.println("anInt = " + anInt);
        }
    }
}
