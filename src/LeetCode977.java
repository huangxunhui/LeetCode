import java.util.Arrays;

/**
 * @author huangxunhui
 * Date: Created in 2020/7/24 1:58 下午
 * Utils: Intellij Idea
 * Description: 977. 有序数组的平方
 */
public class LeetCode977 {

    class Solution {
        public int[] sortedSquares(int[] A) {
            for (int i = 0; i < A.length; i++) {
                A[i] = A[i] * A[i];
            }
            Arrays.sort(A);
            return A;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode977().new Solution();
        int[] A = {-4,-1,0,3,10};
        for (int i : solution.sortedSquares(A)) {
            System.out.println("i = " + i);
        }
    }

}
