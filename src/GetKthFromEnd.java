/**
 * @author huangxunhui
 * Date: Created in 2020/7/21 2:05 下午
 * Utils: Intellij Idea
 * Description: 剑指 Offer 22. 链表中倒数第k个节点
 */
public class GetKthFromEnd {

    class Solution {
        public ListNode getKthFromEnd(ListNode head, int k) {
            ListNode result = null;
            int length = 0;
            ListNode headTmp = head;

            // 获取链表长度
            while (headTmp != null) {
                length++;
                headTmp = headTmp.next;
            }

            for (int i = 0; i <= length - k; i++) {
                result = head;
                head = head.next;
            }

            return result;
        }
    }

}
