/**
 * @author huangxunhui
 * Date: Created in 2019-03-04 20:39
 * Utils: Intellij Idea
 * Description: 258. 各位相加
 */
public class LeetCode258 {

    class Solution {
        public int addDigits(int num) {
            // 如果是个位数 返回
            if (num < 10) {
                return num;
            }
            return addDigits((num / 10) + (num % 10));
        }
    }

    public static void main(String[] args) {
        LeetCode258.Solution solution = new LeetCode258().new Solution();
    }
}
