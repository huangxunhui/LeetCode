/**
 * @author huangxunhui
 * Date: Created in 2019-02-15 14:44
 * Utils: Intellij Idea
 * Description: 27. 移除元素
 */
public class LeetCode_27 {

    class Solution {
        public int removeElement(int[] nums, int val) {
            int p = 0;
            for (int i = 0; i < nums.length; i++) {
                if(nums[i]!= val){
                    nums[p++] = nums[i];
                }
            }
            return p;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode_27().new Solution();
        int[] nums = {3,2,2,3};
        solution.removeElement(nums, 3);
    }

}
