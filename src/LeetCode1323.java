/**
 * @author huangxunhui
 * Date: Created in 2020/7/23 11:25 上午
 * Utils: Intellij Idea
 * Description: 1323. 6 和 9 组成的最大数字
 */
public class LeetCode1323 {

    class Solution {
        public int maximum69Number (int num) {
            int result = num;
            int index = 1000;
            while(num > 0){
                int tmp = num / index;
                if(tmp == 6){
                    return result + (3 * index);
                }
                num %= index;
                index /= 10;
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1323().new Solution();
        System.out.println(solution.maximum69Number(9996));
    }

}
