/**
 * @author huangxunhui
 * Date: Created in 2020/7/23 1:42 下午
 * Utils: Intellij Idea
 * Description: 136. 只出现一次的数字
 */
public class LeetCode136 {

    class Solution {
        public int singleNumber(int[] nums) {
            int  result = 0;
            for (int num : nums) {
                result ^= num;
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode136().new Solution();
        int[] nums = {4,1,2,1,2};
        System.out.println(solution.singleNumber(nums));
    }

}
