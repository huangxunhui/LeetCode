/**
 * @author huangxunhui
 * Date: Created in 2020/7/17 4:36 下午
 * Utils: Intellij Idea
 * Description:
 */
public class LeetCode1295 {

    class Solution {
        public int findNumbers(int[] nums) {
            int result = 0;
            for (int num : nums) {
                int count = 1;
                while (num > 9){
                    num /= 10;
                    count ++;
                }

                if(count % 2 == 0){
                    result ++;
                }
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1295().new Solution();
        int[] nums ={555,901,482,1771};
        System.out.println(solution.findNumbers(nums));
    }

}
