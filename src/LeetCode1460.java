import java.util.Arrays;

/**
 * @author huangxunhui
 * Date: Created in 2020/7/23 4:19 下午
 * Utils: Intellij Idea
 * Description: 1460. 通过翻转子数组使两个数组相等
 */
public class LeetCode1460 {

    class Solution {
        public boolean canBeEqual(int[] target, int[] arr) {
            if(target.length != arr.length){
                return false;
            }
            // 进行排序
            Arrays.sort(arr);
            Arrays.sort(target);
            return Arrays.equals(target,arr);
        }
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode1460().new Solution();
        int[] target = {1,2,3,4};
        int[] arr = {2,4,1,5};
        System.out.println(solution.canBeEqual(target,arr));
    }

}
