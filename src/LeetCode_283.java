/**
 * @author huangxunhui
 * Date: Created in 2019-02-15 14:57
 * Utils: Intellij Idea
 * Description: 283. 移动零
 */
public class LeetCode_283 {

    class Solution {
        public void moveZeroes(int[] nums) {
            for (int i = 0; i < nums.length; i++) {
                if(nums[i] == 0 ){
                    for (int j = i; j < nums.length - 1; j++) {
                        nums[j] = nums[j+1];
                    }
                    nums[nums.length - 1] = 0;
                }
            }
        }
    }

    private void print(int[] nums){
        StringBuilder sb = new StringBuilder();
        for (int num : nums) {
            sb.append(num+"->");
        }
        System.out.println("sb = " + sb.toString());
    }

    public static void main(String[] args) {
        Solution solution = new LeetCode_283().new Solution();
        int[] nums = {0,0,1};
        solution.moveZeroes(nums);
    }

}
