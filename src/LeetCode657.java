/**
 * @author huangxunhui
 * Date: Created in 2020/7/24 11:10 上午
 * Utils: Intellij Idea
 * Description: 657. 机器人能否返回原点
 */
public class LeetCode657 {

    class Solution {

        public boolean judgeCircle(String moves) {

            if (moves == null) {
                return true;
            }

            int x = 0;
            int y = 0;

            for (char c : moves.toCharArray()) {
                switch (c) {
                    case 'U':
                        y++;
                        break;
                    case 'D':
                        y--;
                        break;
                    case 'R':
                        x++;
                        break;
                    default:
                        x--;
                        break;
                }
            }
            return x == 0 && y == 0;
        }
    }


    public static void main(String[] args) {
        Solution solution = new LeetCode657().new Solution();
        String str = "UD";
        System.out.println(solution.judgeCircle(str));
    }
}

